Simulateur du classement WFTDA régional post French Co' 11. 

Cet outil n'est pas approuvé par la WFTDA et est fourni sans aucune garantie, notamment sans garantie que les résultats annoncés par l'outil soient exacts.

Pour le principe de calcul, j'ai utilisé : https://static.wftda.com/rankings/wftda-rankings-policy.pdf
